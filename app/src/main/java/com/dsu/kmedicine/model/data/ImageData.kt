package com.dsu.kmedicine.model.data

import android.net.Uri
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageData(
    @SerializedName("uri")
    val uri: String? = null,
    @SerializedName("base_64")
    val base64: String? = null
) : Parcelable
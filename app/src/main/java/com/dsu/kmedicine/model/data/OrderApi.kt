package com.dsu.kmedicine.model.data

import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.utils.RequestCallback

interface OrderApi {

    fun getOrders(requestCallback: RequestCallback<List<OrderResponse>>)
    fun updateOrder(order: OrderResponse, requestCallback: RequestCallback<List<OrderResponse>>)
    fun setOrders(orders: List<OrderResponse>, requestCallback: RequestCallback<List<OrderResponse>>)
}
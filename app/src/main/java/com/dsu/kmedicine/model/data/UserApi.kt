package com.dsu.kmedicine.model.data

import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.utils.RequestCallback

interface UserApi {

    fun getUser(requestCallback: RequestCallback<UserResponse>)
    fun setUser(user: UserResponse, requestCallback: RequestCallback<UserResponse>)
    fun setNewPersonInCharge(newPersonInChargeName: String, requestCallback: RequestCallback<UserResponse>)

}
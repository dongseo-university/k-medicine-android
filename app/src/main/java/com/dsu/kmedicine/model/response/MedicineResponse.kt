package com.dsu.kmedicine.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class MedicineResponse(
    @SerializedName("id") val packageId: String,
    @SerializedName("name") val name: String,
    @SerializedName("amount") val amount: Int,
    @SerializedName("temperature") var temperature: Int
): Parcelable {
    fun getAmountTemperature(): String {
        return "${amount}개 • ${temperature}°C"
    }
    fun getFormattedTemperature(): String {
        return "${temperature}°C"
    }
}
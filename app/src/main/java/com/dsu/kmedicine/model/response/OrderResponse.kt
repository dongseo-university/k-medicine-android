package com.dsu.kmedicine.model.response

import android.os.Parcelable
import androidx.room.Entity
import com.dsu.kmedicine.utils.InputDataType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Parcelize
data class OrderResponse(
    @SerializedName("id") val id: String,
    @SerializedName("supplier_name") var supplier: String,
    @SerializedName("receiver_name") var receiver: String,
    @SerializedName("description") var description: String,
    @SerializedName("date") var date: String,
    @SerializedName("medicines") var medicinesList: List<MedicineResponse>,
    @SerializedName("transaction_data") var suppliedDataList: MutableList<Map<InputDataType, SuppliedDataResponse>>
): Parcelable {

    fun getFormattedDate(): String {
        val localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"))
        val dateFormat = DateTimeFormatter.ofPattern("yyyy.MM.dd")
        return localDate.format(dateFormat)
    }
}
package com.dsu.kmedicine.model.response

// A generic class that contains data and status about loading this data.
sealed class Response<T>(
    val data: T? = null,
    val error: String? = null
) {
    class Success<T>(data: T) : Response<T>(data)
//    class Loading<T>(data: T? = null) : Resource<T>(data)
    class DataError<T>(error: String) : Response<T>(null, error)

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is DataError -> "Error[exception=$error]"
//            is Loading<T> -> "Loading"
        }
    }
}

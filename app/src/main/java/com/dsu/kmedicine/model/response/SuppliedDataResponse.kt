package com.dsu.kmedicine.model.response

import android.os.Parcelable
import com.dsu.kmedicine.model.data.ImageData
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SuppliedDataResponse(
    @SerializedName("file")
    val pictureList: MutableList<ImageData>,
    @SerializedName("additional_info")
    var additionalInformation: String
) : Parcelable
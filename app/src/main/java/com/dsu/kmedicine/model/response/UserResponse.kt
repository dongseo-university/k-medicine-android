package com.dsu.kmedicine.model.response

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("id") val id: String,
    @SerializedName("password") val password: String,
    @SerializedName("person_in_charge") var personInCharge: String
)
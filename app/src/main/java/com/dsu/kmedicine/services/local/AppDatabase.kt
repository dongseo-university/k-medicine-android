package com.dsu.kmedicine.services.local

import android.content.Context
import android.service.autofill.UserData
import androidx.room.*

@Database(entities = [UserData::class], version = 1)
abstract class AppDatabase (applicationContext: Context) : RoomDatabase()
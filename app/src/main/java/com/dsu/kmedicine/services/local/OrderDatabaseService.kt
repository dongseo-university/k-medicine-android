package com.dsu.kmedicine.services.local

import android.content.Context
import android.content.SharedPreferences
import com.dsu.kmedicine.model.data.OrderApi
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.utils.Constants
import com.dsu.kmedicine.utils.RequestCallback
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class OrderDatabaseService(private val context: Context) : OrderApi {

    private fun getSharedPreferences(): SharedPreferences {
        return context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE)
    }

    private inline fun <reified T> Gson.fromJson(json: String): T =
        fromJson<T>(json, object : TypeToken<T>() {}.type)

    override fun getOrders(requestCallback: RequestCallback<List<OrderResponse>>) = try {
        getSharedPreferences().getString(Constants.ORDER_KEY, "")?.let {
            val orders = Gson().fromJson<List<OrderResponse>>(it)
            requestCallback.onComplete(orders)
        } ?: requestCallback.onException(Exception())
    } catch (e: Exception) {
        requestCallback.onException(e)
    }

    override fun updateOrder(order: OrderResponse, requestCallback: RequestCallback<List<OrderResponse>>) =
        try {
            getOrders(object : RequestCallback<List<OrderResponse>> {
                override fun onComplete(result: List<OrderResponse>) {
                    val mutableResult = result.toMutableList()
                    mutableResult.filter { it.id == order.id }.forEach {
                        it.supplier = order.supplier
                        it.receiver = order.receiver
                        it.description = order.description
                        it.date = order.date
                        it.medicinesList = order.medicinesList
                        it.suppliedDataList = order.suppliedDataList
                    }
                    getSharedPreferences().edit().putString(Constants.ORDER_KEY, Gson().toJson(mutableResult)).apply()
                    requestCallback.onComplete(mutableResult)
                }

                override fun onException(e: Exception?) {
                    requestCallback.onException(e)
                }
            })
        } catch (e: Exception) {
            requestCallback.onException(e)
        }

    override fun setOrders(
        orders: List<OrderResponse>,
        requestCallback: RequestCallback<List<OrderResponse>>
    ) =
        try {
            getSharedPreferences().edit().putString(Constants.ORDER_KEY, Gson().toJson(orders))
                .apply()
            requestCallback.onComplete(orders)
        } catch (e: Exception) {
            requestCallback.onException(e)
        }

}
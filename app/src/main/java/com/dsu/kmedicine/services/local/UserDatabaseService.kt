package com.dsu.kmedicine.services.local

import android.content.Context
import android.content.SharedPreferences
import com.dsu.kmedicine.model.data.UserApi
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.utils.RequestCallback
import com.dsu.kmedicine.utils.Constants
import com.google.gson.Gson

class UserDatabaseService(private val context: Context) : UserApi {

    private fun getSharedPreferences(): SharedPreferences {
        return context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE)
    }

    override fun getUser(requestCallback: RequestCallback<UserResponse>) = try {
        val user = Gson().fromJson(
            getSharedPreferences().getString(Constants.USER_KEY, ""),
            UserResponse::class.java
        )
        requestCallback.onComplete(user)
    } catch (e: Exception) {
        requestCallback.onException(e)
    }

    override fun setUser(user: UserResponse, requestCallback: RequestCallback<UserResponse>) = try {
        getSharedPreferences().edit().putString(Constants.USER_KEY, Gson().toJson(user)).apply()
        requestCallback.onComplete(user)
    } catch (e: Exception) {
        requestCallback.onException(e)
    }

    override fun setNewPersonInCharge(
        newPersonInChargeName: String,
        requestCallback: RequestCallback<UserResponse>
    ) = try {
        val user = Gson().fromJson(
            getSharedPreferences().getString(Constants.USER_KEY, ""),
            UserResponse::class.java
        )
        user.personInCharge = newPersonInChargeName
        setUser(user, requestCallback)
    } catch (e: Exception) {
        requestCallback.onException(e)
    }

}
package com.dsu.kmedicine.services.remote

import com.dsu.kmedicine.model.data.ImageData
import com.dsu.kmedicine.model.data.OrderApi
import com.dsu.kmedicine.model.response.MedicineResponse
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.utils.InputDataType
import com.dsu.kmedicine.utils.RequestCallback
import java.util.*

class OrdersRemoteService : OrderApi {

    override fun getOrders(requestCallback: RequestCallback<List<OrderResponse>>) {
        val ordersList = mutableListOf<OrderResponse>()
        val medicineList = mutableListOf<MedicineResponse>()
        (1..10).forEach { index ->
            medicineList.add(
                MedicineResponse(
                    UUID.randomUUID().toString(),
                    "미리놀과립",
                    index,
                    10 + index
                )
            )
        }
        val transactionDataList = mutableListOf<Map<InputDataType, SuppliedDataResponse>>()
        transactionDataList.add(
            generateMapData(listOf(InputDataType.TAX_INVOICE, InputDataType.TRANSACTION_STATEMENT, InputDataType.TEMPERATURE_INFORMATION, InputDataType.CERTIFICATE_OF_SHIPMENT))
        )
        transactionDataList.add(
            generateMapData(listOf(InputDataType.TAX_INVOICE, InputDataType.TRANSACTION_STATEMENT, InputDataType.TEMPERATURE_INFORMATION))
        )
        transactionDataList.add(
            generateMapData(listOf(InputDataType.TAX_INVOICE, InputDataType.TRANSACTION_STATEMENT, ))
        )
        transactionDataList.add(
            generateMapData(listOf(InputDataType.TAX_INVOICE))
        )
        (1..30).forEach { index ->
            ordersList.add(
                OrderResponse(
                    "20200514-${index}",
                    "행복약품",
                    "행복병원",
                    "항생제 외 3개 품목",
                    "2020-05-14T16:38:30.753",
                    medicineList,
                    transactionDataList
                )
            )
        }
        requestCallback.onComplete(ordersList)
    }

    private fun generateMapData(inputDataTypes: List<InputDataType>): Map<InputDataType, SuppliedDataResponse> {
        val placeholderImageUri =
            ImageData(uri = "android.resource://com.dsu.kmedicine/drawable/invoice_image_placeholder")

        val suppliedData: MutableMap<InputDataType, SuppliedDataResponse> = mutableMapOf()
        inputDataTypes.forEach {
            suppliedData[it] = SuppliedDataResponse(
                mutableListOf(
                    placeholderImageUri,
                    placeholderImageUri,
                    placeholderImageUri,
                    placeholderImageUri,
                ),
                "온도, 참고 및 유의사항 등 부가정보입력"
            )
        }
        return suppliedData
    }

    override fun setOrders(
        orders: List<OrderResponse>,
        requestCallback: RequestCallback<List<OrderResponse>>
    ) {
        TODO("Not yet implemented")
    }

    override fun updateOrder(
        order: OrderResponse,
        requestCallback: RequestCallback<List<OrderResponse>>
    ) {
        TODO("Not yet implemented")
    }

}
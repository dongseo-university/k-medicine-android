package com.dsu.kmedicine.ui.activities

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.dsu.kmedicine.R

abstract class BaseActivity : AppCompatActivity() {

    private val cameraRequestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                showMessage(resources.getString(R.string.external_storage_permission_denied_message))
            } else {
                onGrantedPermission?.invoke()
            }
        }
    private val externalStorageRequestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                showMessage(resources.getString(R.string.camera_permission_denied_message))
            } else {
                onGrantedPermission?.invoke()
            }
        }
    private var onGrantedPermission: (() -> Unit)? = null

    protected abstract fun initViewModel()
    protected abstract fun initViewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViewModel()
        initViewBinding()
        if (this::class != HomeActivity::class) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    protected fun checkCameraPermission(onGrantedPermission: () -> Unit) {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            this.onGrantedPermission = onGrantedPermission
            cameraRequestPermissionLauncher.launch(Manifest.permission.CAMERA)
        } else {
            onGrantedPermission()
        }
    }

    protected fun checkExternalStoragePermission(onGrantedPermission: () -> Unit) {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            this.onGrantedPermission = onGrantedPermission
            externalStorageRequestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        } else {
            onGrantedPermission()
        }
    }

    protected fun showMessage(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
package com.dsu.kmedicine.ui.activities

import androidx.lifecycle.ViewModelProvider
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivityChangePersonInChargeBinding
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.ui.viewmodels.ChangePersonInChargeViewModel
import com.dsu.kmedicine.utils.observe

class ChangePersonInChargeActivity : BaseActivity() {

    private lateinit var binding: ActivityChangePersonInChargeBinding
    private lateinit var viewModel: ChangePersonInChargeViewModel

    override fun initViewBinding() {
        binding = ActivityChangePersonInChargeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.elevation = 0f
        binding.confirmButton.setOnClickListener {
            viewModel.setNewPersonInCharge(binding.newText.text.toString())
        }
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this)[ChangePersonInChargeViewModel::class.java]
        observe(viewModel.user, ::onSetNewRequestCompleted)
        observe(viewModel.previousPersonInCharge, ::onLoadedPersonInCharge)
        viewModel.checkUserExistence()
    }

    private fun onSetNewRequestCompleted(user: Response<UserResponse>) {
        user.data?.let {
            showMessage(resources.getString(R.string.person_in_charge_complete))
        }
        user.error?.let {
            if (it.isNotEmpty()) showMessage(resources.getString(R.string.general_server_error))
            else showMessage(resources.getString(R.string.person_in_charge_field_error))
        }
        finish()
    }

    private fun onLoadedPersonInCharge(newPersonInCharge: Response<String>) {
        newPersonInCharge.data?.let {
            binding.previousText.setText(it)
        } ?: run {
            showMessage(resources.getString(R.string.person_in_charge_field_error))
            finish()
        }

    }
}
package com.dsu.kmedicine.ui.activities

import android.content.Intent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivityHomeBinding
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.ui.adapters.OrdersListAdapter
import com.dsu.kmedicine.ui.viewmodels.HomeViewModel
import com.dsu.kmedicine.utils.Constants
import com.dsu.kmedicine.utils.observe
import io.github.g00fy2.quickie.QRResult
import io.github.g00fy2.quickie.ScanCustomCode
import io.github.g00fy2.quickie.config.ScannerConfig

class HomeActivity : BaseActivity() {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var adapter: OrdersListAdapter
    private val scanQrCode = registerForActivityResult(ScanCustomCode(), ::handleQRCodeResult)

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun initViewBinding() {
        setTheme(R.style.Theme_KMedicine)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupSearchViewListener()
        setupFABListener()
        setupRecyclerView()
        viewModel.checkUserExistence()
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        observe(viewModel.user, ::onUserExistenceChecked)
        observe(viewModel.orders, ::onOrdersRetrieved)
        observe(viewModel.openOrderDetails, ::navigateToOrderDetails)
    }

    private fun setupRecyclerView() {
        adapter = OrdersListAdapter(viewModel)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }

    private fun onUserExistenceChecked(user: Response<UserResponse>) {
        user.data?.let {
            viewModel.retrieveDataList()
        } ?: navigateToSignUp()
    }

    private fun setupSearchViewListener() {
        binding.searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(
                    query: String?
                ): Boolean {
                    viewModel.search(query.orEmpty())
                    binding.searchView.clearFocus()
                    return true
                }

                override fun onQueryTextChange(
                    newText: String?
                ): Boolean {
                    viewModel.search(newText.orEmpty())
                    return true
                }
            }
        )
    }

    private fun setupFABListener() {
        binding.scanQRCodeView.setOnClickListener {
            scanQrCode.launch(
                ScannerConfig.build {
                    setOverlayStringRes(R.string.qr_code_scan_message)
                }
            )
        }
    }

    private fun handleQRCodeResult(result: QRResult) {
        if (result is QRResult.QRSuccess) {
            showMessage(result.content.rawValue)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        navigateToChangePersonInCharge()
        return true
    }

    private fun onOrdersRetrieved(list: Response<List<OrderResponse>>) {
        list.data?.let {
            adapter.setOrdersList(it)
        }
    }

    private fun navigateToChangePersonInCharge() {
        val intent = Intent(this, ChangePersonInChargeActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToSignUp() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun navigateToOrderDetails(orderResponse: Response<OrderResponse>) {
        orderResponse.data?.let {
            val nextScreenIntent = Intent(this, OrderDetailActivity::class.java).apply {
                putExtra(
                    Constants.ORDER_KEY, it
                )
            }
            startActivity(nextScreenIntent)
        }
    }
}
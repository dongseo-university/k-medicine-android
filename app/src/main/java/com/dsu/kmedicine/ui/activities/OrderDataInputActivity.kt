package com.dsu.kmedicine.ui.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivityOrderDataInputBinding
import com.dsu.kmedicine.model.data.ImageData
import com.dsu.kmedicine.model.response.MedicineResponse
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.ui.adapters.DataInputPictureAdapter
import com.dsu.kmedicine.ui.adapters.MedicineTemperatureInfoListAdapter
import com.dsu.kmedicine.ui.viewmodels.OrderDataInputViewModel
import com.dsu.kmedicine.utils.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.io.Serializable

class OrderDataInputActivity : BaseActivity() {

    private lateinit var binding: ActivityOrderDataInputBinding
    private lateinit var viewModel: OrderDataInputViewModel
    private lateinit var pictureAdapter: DataInputPictureAdapter
    private lateinit var medTempInfoAdapter: MedicineTemperatureInfoListAdapter

    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            onCameraResult(it)
        }
    private val photoPickedResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            onPhotoPickedResult(it)
        }

    override fun onResume() {
        hideBottomSheet()
        super.onResume()
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this)[OrderDataInputViewModel::class.java]
        val order = intent.extras?.getParcelable(Constants.ORDER_KEY) as? OrderResponse
        order?.medicinesList?.let {
            viewModel.medicines.value = it
        }
        val inputDataType =
            intent.extras?.getSerializable(Constants.INPUT_DATA_TYPE) as? InputDataType
        inputDataType?.let {
            viewModel.inputDataType.value = it
        }
        val suppliedData = intent.extras?.getParcelable(Constants.SUPPLIED_DATA) as? SuppliedDataResponse
        suppliedData?.let {
            viewModel.suppliedData.value = it
        }
        val isEditable = intent.extras?.getSerializable(Constants.IS_INPUT_DATA_EDITABLE) as? Boolean
        isEditable?.let {
            viewModel.isEditable = it
        }
        observe(viewModel.medicines, ::onMedicinesRetrieved)
        observe(viewModel.suppliedData, ::onSuppliedDataRetrieved)
        observe(viewModel.inputDataType, ::onInputDataTypeRetrieved)
        observe(viewModel.cameraClickEvent, ::onAddPhotoClick)
        observe(viewModel.imageClickEvent, ::showImageExpanded)
    }

    override fun initViewBinding() {
        binding = ActivityOrderDataInputBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewModel = viewModel
        binding.additionalInformation.setRawInputType(InputType.TYPE_CLASS_TEXT)
        setUpBottomSheet()
        setUpRecyclerViews()
        binding.expandedImage.closeButton.setOnClickListener { hideImageExpanded() }
        if (!viewModel.isEditable) {
            binding.additionalInformation.isEnabled = false
            binding.additionalInformation.hint = ""
        }
    }

    private fun setUpBottomSheet() {
        binding.bottomSheetContent.cameraLayout.setOnClickListener { openCamera() }
        binding.bottomSheetContent.imagePickerLayout.setOnClickListener { openPhotoPicker() }
        binding.overlayView.setOnClickListener { hideBottomSheet() }
        hideBottomSheet()
    }

    private fun setUpRecyclerViews() {
        pictureAdapter = DataInputPictureAdapter(viewModel)
        val layoutManager = LinearLayoutManager(this)
        binding.picturesRecyclerView.layoutManager = layoutManager
        layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.picturesRecyclerView.adapter = pictureAdapter

        medTempInfoAdapter = MedicineTemperatureInfoListAdapter(viewModel)
        binding.medicineTemperatureRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.medicineTemperatureRecyclerView.adapter = medTempInfoAdapter
    }

    private fun onMedicinesRetrieved(medicinesList: List<MedicineResponse>) {
        medTempInfoAdapter.setList(medicinesList.toMutableList(), viewModel.isEditable)
    }

    private fun onSuppliedDataRetrieved(suppliedResponse: SuppliedDataResponse) {
        pictureAdapter.setList(suppliedResponse.pictureList, viewModel.isEditable)
        viewModel.additionalInformation.value = suppliedResponse.additionalInformation
    }

    private fun onInputDataTypeRetrieved(inputDataType: InputDataType) {
        if (inputDataType == InputDataType.CERTIFICATE_OF_SHIPMENT) {
            binding.medicineTemperatureListTitle.isVisible = true
            binding.medicineTemperatureRecyclerView.isVisible = true
        }
    }

    private fun onAddPhotoClick(pictureList: MutableList<ImageData>) {
        openTakeOrChoosePhotoOptions()
    }

    private fun openTakeOrChoosePhotoOptions() {
        showBottomSheet()
    }

    private fun showBottomSheet() {
        BottomSheetBehavior.from(binding.bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
        binding.overlayView.visibility = View.VISIBLE
    }

    private fun hideBottomSheet() {
        BottomSheetBehavior.from(binding.bottomSheet).state = BottomSheetBehavior.STATE_HIDDEN
        binding.overlayView.visibility = View.GONE
    }

    private fun showImageExpanded(img: ImageData) {
        img.uri?.apply {
            binding.expandedImage.image.setImageURI(this.toUri())
        }
        img.base64?.apply {
            binding.expandedImage.image.setImageBitmap(this.toBitmap())
        }
        binding.expandedImage.root.visibility = View.VISIBLE
    }

    private fun hideImageExpanded() {
        binding.expandedImage.root.visibility = View.GONE
    }

    private fun openCamera() {
        checkCameraPermission {
            cameraResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
        }
    }

    private fun openPhotoPicker() {
        checkExternalStoragePermission {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            photoPickedResult.launch(intent)
        }
    }

    private fun onCameraResult(it: ActivityResult) {
        if (it.resultCode == RESULT_OK) {
            val imageBitmap = it.data?.extras?.get("data") as Bitmap
            imageBitmap.toBase64()?.let {
                viewModel.addNewImageToTransaction(ImageData(base64 = it))
            }
            //TODO save on phone
        }
    }

    private fun onPhotoPickedResult(it: ActivityResult) {
        if (it.resultCode == RESULT_OK) {
            it.data?.data?.let {
                BitmapFactory.decodeStream(contentResolver.openInputStream(it)).toBase64()?.run {
                    viewModel.addNewImageToTransaction(ImageData(base64 = this))
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (viewModel.isEditable) {
            val inflater: MenuInflater = menuInflater
            inflater.inflate(R.menu.menu_order_data_input, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_save) {
            onSaveSelected()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onSaveSelected() {
        val additionalInfo = binding.additionalInformation.text.toString()
        // No picture
        if(viewModel.suppliedData.value == null && additionalInfo.isNotEmpty()) {
            viewModel.suppliedData.value = SuppliedDataResponse(mutableListOf(), additionalInfo)
        } else {
            viewModel.suppliedData.value?.additionalInformation = additionalInfo
        }
        viewModel.medicines.value = medTempInfoAdapter.temperatureInfoList
        goBackToPreviousActivity(additionalInfo)
    }

    private fun goBackToPreviousActivity(additionalInfo: String) {
        val resultIntent = Intent()
        resultIntent.putExtra(Constants.INPUT_DATA_TYPE, viewModel.inputDataType.value)
        resultIntent.putExtra(Constants.SUPPLIED_DATA, viewModel.suppliedData.value)
        resultIntent.putExtra(Constants.MEDICINE_KEY, viewModel.medicines.value as Serializable)
        setResult(RESULT_OK, resultIntent)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        return if (binding.expandedImage.root.visibility == View.VISIBLE) {
            hideImageExpanded()
            true
        } else {
            super.onSupportNavigateUp()
        }
    }

}
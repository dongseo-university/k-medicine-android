package com.dsu.kmedicine.ui.activities

import android.content.Intent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivityOrderDataInputOptionsBinding
import com.dsu.kmedicine.model.response.MedicineResponse
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.ui.viewmodels.OrderDataInputOptionsViewModel
import com.dsu.kmedicine.utils.Constants
import com.dsu.kmedicine.utils.InputDataType
import com.dsu.kmedicine.utils.observe

class OrderDataInputOptionsActivity : BaseActivity() {

    private lateinit var binding: ActivityOrderDataInputOptionsBinding
    private lateinit var optionsViewModel: OrderDataInputOptionsViewModel
    private val dataInputResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        onDataInputResult(result)
    }
    private var newSuppliedData = mutableMapOf<InputDataType, SuppliedDataResponse>()

    override fun initViewModel() {
        optionsViewModel = ViewModelProvider(this)[OrderDataInputOptionsViewModel::class.java]
        val order = intent.extras?.getParcelable(Constants.ORDER_KEY) as? OrderResponse
        order?.apply {
            optionsViewModel.order.value = Response.Success(this)
        }
        observe(optionsViewModel.order, ::onOrderRetrieved)
    }

    override fun initViewBinding() {
        binding = ActivityOrderDataInputOptionsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.taxInvoice.icon.setImageResource(R.drawable.ic_tax_invoice)
        binding.taxInvoice.title.setText(R.string.tax_invoice)
        binding.taxInvoice.cardView.setCardBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.blue_gray
            )
        )
        binding.taxInvoice.root.setOnClickListener { navigateToDataInputActivity(InputDataType.TAX_INVOICE) }

        binding.transactionStatement.icon.setImageResource(R.drawable.ic_transaction_statement)
        binding.transactionStatement.title.setText(R.string.transaction_statement)
        binding.transactionStatement.cardView.setCardBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.blue_400
            )
        )
        binding.transactionStatement.root.setOnClickListener { navigateToDataInputActivity(InputDataType.TRANSACTION_STATEMENT) }

        binding.temperatureInformation.icon.setImageResource(R.drawable.ic_temperature)
        binding.temperatureInformation.title.setText(R.string.temperature_information)
        binding.temperatureInformation.cardView.setCardBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.primaryColor
            )
        )
        binding.temperatureInformation.root.setOnClickListener { navigateToDataInputActivity(InputDataType.TEMPERATURE_INFORMATION) }

        binding.certificateOfShipment.icon.setImageResource(R.drawable.ic_shipment)
        binding.certificateOfShipment.title.setText(R.string.certificate_of_shipment)
        binding.certificateOfShipment.cardView.setCardBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.primaryDarkColor
            )
        )
        binding.certificateOfShipment.root.setOnClickListener { navigateToDataInputActivity(InputDataType.CERTIFICATE_OF_SHIPMENT) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_order_data_options_input, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_complete) {
            navigateToDistributionActivity()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onOrderRetrieved(orderResponse: Response<OrderResponse>) {
        orderResponse.data?.apply {
            binding.topLayout.title.text = description
            binding.topLayout.supplier.text = supplier
            binding.topLayout.receiver.text = receiver
            binding.topLayout.date.text = getFormattedDate()
        }
    }

    private fun navigateToDataInputActivity(inputDataType: InputDataType) {
        optionsViewModel.order.value?.data?.let { order ->
            val nextScreenIntent = Intent(this, OrderDataInputActivity::class.java).apply {
                putExtra(Constants.ORDER_KEY, order)
                putExtra(Constants.INPUT_DATA_TYPE, inputDataType)
                if (newSuppliedData.containsKey(inputDataType)) {
                    putExtra(Constants.SUPPLIED_DATA, newSuppliedData[inputDataType])
                }
                putExtra(Constants.IS_INPUT_DATA_EDITABLE, true)
            }
            dataInputResult.launch(nextScreenIntent)
        }
    }

    private fun onDataInputResult(result: ActivityResult) {
        if (result.resultCode == RESULT_OK) {
            result.data?.extras?.let { bundle ->
                val suppliedData =
                    bundle.getParcelable(Constants.SUPPLIED_DATA) as? SuppliedDataResponse
                val inputDataType =
                    bundle.getSerializable(Constants.INPUT_DATA_TYPE) as? InputDataType
                val medicines =
                    bundle.getSerializable(Constants.MEDICINE_KEY) as? List<MedicineResponse>
                setNewData(inputDataType, suppliedData, medicines)
            }
        }
    }

    private fun setNewData(
        inputDataType: InputDataType?,
        suppliedData: SuppliedDataResponse?,
        medicines: List<MedicineResponse>?
    ) {
        if (inputDataType != null && suppliedData != null) {
            newSuppliedData[inputDataType] = suppliedData
        }
        medicines?.let {
            optionsViewModel.order.value?.data?.medicinesList = it
        }
    }

    private fun navigateToDistributionActivity() {
        if (newSuppliedData.isNotEmpty()) {
            optionsViewModel.order.value?.data?.suppliedDataList?.add(newSuppliedData)
        }
        val nextScreenIntent = Intent(this, StepDistributionActivity::class.java).apply {
            putExtra(Constants.ORDER_KEY, optionsViewModel.order.value?.data)
        }
        startActivity(nextScreenIntent)
        finish()
    }
}
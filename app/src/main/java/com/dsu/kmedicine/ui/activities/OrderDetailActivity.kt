package com.dsu.kmedicine.ui.activities

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dsu.kmedicine.databinding.ActivityOrderDetailBinding
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.ui.adapters.OrderDetailListAdapter
import com.dsu.kmedicine.ui.viewmodels.OrderDetailViewModel
import com.dsu.kmedicine.utils.Constants
import com.dsu.kmedicine.utils.observe

class OrderDetailActivity : BaseActivity() {

    private lateinit var binding: ActivityOrderDetailBinding
    private lateinit var viewModel: OrderDetailViewModel
    private lateinit var adapter: OrderDetailListAdapter

    override fun initViewModel() {
        viewModel = ViewModelProvider(this)[OrderDetailViewModel::class.java]
        val order = intent.extras?.getParcelable(Constants.ORDER_KEY) as? OrderResponse
        order?.apply {
            viewModel.order.value = Response.Success(this)
        }
        observe(viewModel.order, ::onOrderRetrieved)
    }

    override fun initViewBinding() {
        binding = ActivityOrderDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()
        binding.confirmButton.setOnClickListener {
            navigateToOrderDataInputActivity(viewModel.order.value)
        }
    }

    private fun onOrderRetrieved(orderResponse: Response<OrderResponse>) {
        orderResponse.data?.apply {
            binding.topLayout.title.text = this.description
            binding.topLayout.supplier.text = this.supplier
            binding.topLayout.receiver.text = this.receiver
            binding.topLayout.date.text = this.getFormattedDate()
            adapter.setOrdersList(this.medicinesList)
        }
    }

    private fun setupRecyclerView() {
        adapter = OrderDetailListAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }

    private fun navigateToOrderDataInputActivity(orderResponse: Response<OrderResponse>?) {
        orderResponse?.data?.let {
            val nextScreenIntent = Intent(this, OrderDataInputOptionsActivity::class.java).apply {
                putExtra(
                    Constants.ORDER_KEY, it
                )
            }
            startActivity(nextScreenIntent)
        }
    }
}
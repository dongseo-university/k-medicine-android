package com.dsu.kmedicine.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivitySignUpBinding
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.ui.viewmodels.SignUpViewModel
import com.dsu.kmedicine.utils.observe

class SignUpActivity : BaseActivity() {

    private lateinit var viewModel: SignUpViewModel
    private lateinit var binding: ActivitySignUpBinding

    override fun initViewModel() {
        viewModel = ViewModelProvider(this)[SignUpViewModel::class.java]
        observe(viewModel.user, ::onSignUpRequestCompleted)
    }

    override fun initViewBinding() {
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.confirmButton.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        viewModel.signUp(
            binding.idText.text?.trim().toString(),
            binding.passwordText.text?.trim().toString(),
            binding.personInChargeText.text.toString()
        )

    }

    private fun onSignUpRequestCompleted(user: Response<UserResponse>) {
        user.data?.let {
            goHome()
        }
        user.error?.let {
            if (it.isNotEmpty()) showMessage(resources.getString(R.string.general_server_error))
                else showMessage(resources.getString(R.string.sign_up_field_error))
        }

    }

    private fun goHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}
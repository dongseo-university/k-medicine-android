package com.dsu.kmedicine.ui.activities

import android.content.Intent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivitySignatureBinding
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.services.local.OrderDatabaseService
import com.dsu.kmedicine.ui.viewmodels.SignatureViewModel
import com.dsu.kmedicine.utils.Constants
import com.dsu.kmedicine.utils.clearStack
import com.dsu.kmedicine.utils.toBase64

class SignatureActivity : BaseActivity() {
    private lateinit var binding: ActivitySignatureBinding
    private lateinit var viewModel: SignatureViewModel

    override fun initViewModel() {
        viewModel = ViewModelProvider(this)[SignatureViewModel::class.java]
        val order = intent.extras?.getParcelable(Constants.ORDER_KEY) as? OrderResponse
        order?.let {
            viewModel.order.value = Response.Success(it)
        }
    }

    override fun initViewBinding() {
        binding = ActivitySignatureBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.clearPadButton.setOnClickListener { clearPad() }
    }

    private fun clearPad() {
        binding.signaturePad.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_signature, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_save) {
            onCompleteSelected()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onCompleteSelected() {
        if (binding.signaturePad.isEmpty) {
            showMessage(resources.getString(R.string.signature_screen_empty_signature_pad))
        } else {
            persistOnDatabase()
        }
    }

    private fun persistOnDatabase() {
        binding.signaturePad.getSignatureBitmap().toBase64()?.let {
            viewModel.updateOrderOnDatabase(OrderDatabaseService(this))
        }
        navigateToHome()
    }

    private fun navigateToHome() {
        val nextActivityIntent = Intent(this, HomeActivity::class.java)
        nextActivityIntent.clearStack()
        startActivity(nextActivityIntent)
        finish()
    }
}
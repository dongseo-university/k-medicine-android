package com.dsu.kmedicine.ui.activities

import android.content.Intent
import android.view.MenuItem
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.ActivityStepDistributionBinding
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.ui.adapters.StepDistributionPageAdapter
import com.dsu.kmedicine.ui.viewmodels.StepDistributionViewModel
import com.dsu.kmedicine.utils.Constants
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class StepDistributionActivity : BaseActivity() {

    private lateinit var binding: ActivityStepDistributionBinding
    private val viewModel: StepDistributionViewModel by viewModels()

    override fun initViewModel() {
        val order = intent.extras?.getParcelable(Constants.ORDER_KEY) as? OrderResponse
        order?.apply {
            viewModel.order.value = Response.Success(this)
        }
    }

    override fun initViewBinding() {
        binding = ActivityStepDistributionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewPager.adapter = StepDistributionPageAdapter(viewModel.order.value?.data?.suppliedDataList ?: mutableListOf(), this)
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = "${(position + 1)} 차"
        }.attach()
        binding.confirmButton.setOnClickListener { onConfirmButtonClicked() }
    }

    private fun onConfirmButtonClicked() {
        viewModel.order.value?.data?.let {
            val nextScreenIntent = Intent(this, SignatureActivity::class.java).apply {
                putExtra(
                    Constants.ORDER_KEY, it
                )
            }
            startActivity(nextScreenIntent)
        }
    }
}
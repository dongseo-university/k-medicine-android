package com.dsu.kmedicine.ui.adapters

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dsu.kmedicine.databinding.DataInputPictureHeaderBinding
import com.dsu.kmedicine.databinding.DataInputPictureItemBinding
import com.dsu.kmedicine.model.data.ImageData
import com.dsu.kmedicine.ui.viewmodels.OrderDataInputViewModel
import com.dsu.kmedicine.utils.toBitmap

private const val PICTURE_VIEW_TYPE_HEADER = 0
private const val PICTURE_VIEW_TYPE_ITEM = 1

class DataInputPictureAdapter(private val viewModel: OrderDataInputViewModel) :
    ListAdapter<ImageData, RecyclerView.ViewHolder>(DataInputPictureCallback()) {
    private var pictureList = mutableListOf<ImageData>()
    private var hasHeader = true

    fun setList(pictureList: MutableList<ImageData>, hasHeader: Boolean) {
        this.pictureList = pictureList.toMutableList()
        this.hasHeader = hasHeader
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (hasHeader) {
            if (pictureList.isEmpty()) 1 else pictureList.size + 1
        } else {
            pictureList.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasHeader) {
            if (position == 0) PICTURE_VIEW_TYPE_HEADER else PICTURE_VIEW_TYPE_ITEM
        } else PICTURE_VIEW_TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            PICTURE_VIEW_TYPE_HEADER -> {
                DataInputPictureHeaderViewHolder(
                    DataInputPictureHeaderBinding.inflate(
                        inflater,
                        parent,
                        false
                    )
                )
            }
            PICTURE_VIEW_TYPE_ITEM -> {
                DataInputPictureViewHolder(
                    DataInputPictureItemBinding.inflate(
                        inflater,
                        parent,
                        false
                    )
                )
            }
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DataInputPictureHeaderViewHolder -> {
                if (pictureList.isNotEmpty()) {
                    holder.bindWithPictureNumber(pictureList.size)
                } else {
                    holder.bindWithoutPictureNumber()
                }
                holder.binding.cardView.setOnClickListener { viewModel.onCameraClick(pictureList) }
            }
            is DataInputPictureViewHolder -> {
                val pos = if (hasHeader) position - 1 else position
                val img = pictureList[pos]
                img.uri?.apply {
                    holder.binding.pictureMiniature.setImageURI(this.toUri())
                }
                img.base64?.apply {
                    holder.binding.pictureMiniature.setImageBitmap(this.toBitmap())
                }
                holder.binding.cardView.setOnClickListener { viewModel.onImageClick(img) }
            }
        }
    }

    class DataInputPictureHeaderViewHolder(val binding: DataInputPictureHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindWithPictureNumber(listSize: Int) {
            binding.pictureNumber.visibility = View.VISIBLE
            binding.pictureNumber.text = "$listSize"
        }

        fun bindWithoutPictureNumber() {
            binding.pictureNumber.visibility = View.GONE
        }
    }

    class DataInputPictureViewHolder(val binding: DataInputPictureItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}

class DataInputPictureCallback : DiffUtil.ItemCallback<ImageData>() {
    override fun areItemsTheSame(oldItem: ImageData, newItem: ImageData): Boolean {
        return oldItem == newItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: ImageData, newItem: ImageData): Boolean {
        return oldItem == newItem
    }
}
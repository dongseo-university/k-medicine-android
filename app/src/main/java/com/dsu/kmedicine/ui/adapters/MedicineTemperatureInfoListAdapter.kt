package com.dsu.kmedicine.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dsu.kmedicine.databinding.MedicineTemperatureInformationItemBinding
import com.dsu.kmedicine.model.response.MedicineResponse
import com.dsu.kmedicine.ui.viewmodels.OrderDataInputViewModel

class MedicineTemperatureInfoListAdapter(val viewModel: OrderDataInputViewModel) :
    RecyclerView.Adapter<MedicineTemperatureInfoListAdapter.MedTempInfoViewHolder>() {
    var temperatureInfoList = mutableListOf<MedicineResponse>()
    private var isEditable = true

    fun setList(temperatureInfoList: MutableList<MedicineResponse>, isEditable: Boolean) {
        this.temperatureInfoList = temperatureInfoList
        this.isEditable = isEditable
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return temperatureInfoList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedTempInfoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MedicineTemperatureInformationItemBinding.inflate(inflater, parent, false)
        if (!isEditable) {
            binding.plusButton.visibility = View.INVISIBLE
            binding.minusButton.visibility = View.INVISIBLE
        }
        return MedTempInfoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MedTempInfoViewHolder, position: Int) {
        val item = temperatureInfoList[position]
        holder.binding.medicineName.text = item.name
        holder.binding.temperature.text = item.getFormattedTemperature()
        holder.binding.plusButton.setOnClickListener {
            onPlusClicked(holder, position)
        }
        holder.binding.minusButton.setOnClickListener {
            onMinusClicked(holder, position)
        }
    }

    private fun onPlusClicked(holder: MedTempInfoViewHolder, position: Int) {
        temperatureInfoList[position].temperature += 1
        holder.binding.temperature.text = temperatureInfoList[position].getFormattedTemperature()
    }

    private fun onMinusClicked(holder: MedTempInfoViewHolder, position: Int) {
        temperatureInfoList[position].temperature -= 1
        holder.binding.temperature.text = temperatureInfoList[position].getFormattedTemperature()
    }

    class MedTempInfoViewHolder(val binding: MedicineTemperatureInformationItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}
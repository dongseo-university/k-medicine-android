package com.dsu.kmedicine.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dsu.kmedicine.databinding.MedicineItemBinding
import com.dsu.kmedicine.model.response.MedicineResponse

class OrderDetailListAdapter : RecyclerView.Adapter<MedicineItemViewHolder>() {
    private var medicineList = mutableListOf<MedicineResponse>()

    fun setOrdersList(medicines: List<MedicineResponse>) {
        this.medicineList = medicines.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return medicineList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MedicineItemBinding.inflate(inflater, parent, false)
        return MedicineItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MedicineItemViewHolder, position: Int) {
        val medicine = medicineList[position]
        holder.binding.id.text = medicine.packageId
        holder.binding.name.text = medicine.name
        holder.binding.amountTemperature.text = medicine.getAmountTemperature()
    }

}

class MedicineItemViewHolder(val binding: MedicineItemBinding) :
    RecyclerView.ViewHolder(binding.root)
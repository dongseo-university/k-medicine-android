package com.dsu.kmedicine.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dsu.kmedicine.databinding.OrderItemBinding
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.ui.viewmodels.HomeViewModel

class OrdersListAdapter(private val homeViewModel: HomeViewModel) :
    RecyclerView.Adapter<OrderListItemViewHolder>() {

    var orders = mutableListOf<OrderResponse>()

    fun setOrdersList(orders: List<OrderResponse>) {
        this.orders = orders.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = OrderItemBinding.inflate(inflater, parent, false)
        return OrderListItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderListItemViewHolder, position: Int) {
        val order = orders[position]
        holder.binding.title.text = order.id
        holder.binding.supplier.text = order.supplier
        holder.binding.receiver.text = order.receiver
        holder.binding.description.text = order.description
        holder.binding.cardView.setOnClickListener { homeViewModel.openOrderDetails(order) }
    }

}

class OrderListItemViewHolder(val binding: OrderItemBinding) :
    RecyclerView.ViewHolder(binding.root)
package com.dsu.kmedicine.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.DistributionDataItemBinding
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.utils.InputDataType
import com.dsu.kmedicine.utils.toBitmap

class StepDistributionListAdapter(
    private val suppliedDataKey: List<InputDataType>,
    private val suppliedDataValue: List<SuppliedDataResponse>,
    private val onItemClick: (itemType: InputDataType, itemValue: SuppliedDataResponse) -> Unit
) :
    RecyclerView.Adapter<SetDistributionViewHolder>() {

    override fun getItemCount(): Int {
        return suppliedDataKey.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SetDistributionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DistributionDataItemBinding.inflate(inflater, parent, false)
        return SetDistributionViewHolder(binding)
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: SetDistributionViewHolder, position: Int) {
        val suppliedDataType = suppliedDataKey[position]
        val suppliedDataValue = suppliedDataValue[position]
        val resources = holder.binding.root.context.resources
        val title = resources.getString(
            R.string.input_data_type_image_title,
            resources.getString(suppliedDataType.drawableRes),
            suppliedDataValue.pictureList.size
        )
        holder.binding.title.text = title
        holder.binding.description.text = suppliedDataValue.additionalInformation
        if (suppliedDataValue.pictureList.isNotEmpty()) {
            val picture = suppliedDataValue.pictureList[0]
            picture.uri?.apply {
                holder.binding.representativeImage.setImageURI(this.toUri())
            }
            picture.base64?.apply {
                holder.binding.representativeImage.setImageBitmap(this.toBitmap())
            }
        }
        holder.binding.root.setOnClickListener { onItemClick(suppliedDataType, suppliedDataValue) }
    }

}

class SetDistributionViewHolder(val binding: DistributionDataItemBinding) :
    RecyclerView.ViewHolder(binding.root)
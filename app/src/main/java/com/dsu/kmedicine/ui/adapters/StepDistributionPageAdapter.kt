package com.dsu.kmedicine.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.ui.fragments.StepDistributionListFragment
import com.dsu.kmedicine.utils.InputDataType

class StepDistributionPageAdapter(private val suppliedDataList: List<Map<InputDataType, SuppliedDataResponse>>, fa: FragmentActivity): FragmentStateAdapter(fa) {

    override fun getItemCount(): Int {
        return suppliedDataList.size
    }

    override fun createFragment(position: Int): Fragment {
        return StepDistributionListFragment(suppliedDataList[position])
    }
}
package com.dsu.kmedicine.ui.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.dsu.kmedicine.R
import com.dsu.kmedicine.databinding.FragmentStepDistributionListBinding
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.ui.activities.OrderDataInputActivity
import com.dsu.kmedicine.ui.adapters.StepDistributionListAdapter
import com.dsu.kmedicine.ui.viewmodels.StepDistributionViewModel
import com.dsu.kmedicine.utils.Constants
import com.dsu.kmedicine.utils.InputDataType

class StepDistributionListFragment(private val suppliedData: Map<InputDataType, SuppliedDataResponse>) : Fragment() {

    private lateinit var binding: FragmentStepDistributionListBinding
    private val viewModel: StepDistributionViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStepDistributionListBinding.inflate(layoutInflater)
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        binding.recyclerView.adapter = StepDistributionListAdapter(suppliedData.keys.toList(), suppliedData.values.toList()) { itemType, itemValue ->
            navigateToDataInputActivity(itemType, itemValue)
        }
        return binding.root
    }

    private fun navigateToDataInputActivity(inputDataType: InputDataType, itemValue: SuppliedDataResponse) {
        viewModel.order.value?.data?.let { order ->
            val nextScreenIntent = Intent(activity, OrderDataInputActivity::class.java).apply {
                putExtra(Constants.ORDER_KEY, order)
                putExtra(Constants.INPUT_DATA_TYPE, inputDataType)
                putExtra(Constants.SUPPLIED_DATA, itemValue)
                putExtra(Constants.IS_INPUT_DATA_EDITABLE, false)
            }
            startActivity(nextScreenIntent)
        }
    }
}
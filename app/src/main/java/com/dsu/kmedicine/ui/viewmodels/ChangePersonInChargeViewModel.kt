package com.dsu.kmedicine.ui.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.services.local.UserDatabaseService
import com.dsu.kmedicine.utils.RequestCallback

class ChangePersonInChargeViewModel(application: Application): AndroidViewModel(application) {

    val user = MutableLiveData<Response<UserResponse>>()
    val previousPersonInCharge = MutableLiveData<Response<String>>()

    fun checkUserExistence() {
        with(UserDatabaseService(getApplication())) {
            getUser(object : RequestCallback<UserResponse> {
                override fun onComplete(result: UserResponse) {
                    previousPersonInCharge.value = Response.Success(result.personInCharge)
                }

                override fun onException(e: Exception?) {
                    previousPersonInCharge.value = Response.DataError(e.toString())
                }
            })
        }
    }

    fun setNewPersonInCharge(newPersonInCharge: String) {
        if (newPersonInCharge.isNotEmpty()) {
            with(UserDatabaseService(getApplication())) {
                setNewPersonInCharge(newPersonInCharge, object : RequestCallback<UserResponse> {
                    override fun onComplete(result: UserResponse) {
                        user.value = Response.Success(result)
                    }

                    override fun onException(e: Exception?) {
                        user.value = Response.DataError(e.toString())
                    }
                })
            }
        } else user.value = Response.DataError("")
    }

}
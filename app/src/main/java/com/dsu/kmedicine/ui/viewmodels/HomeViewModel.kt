package com.dsu.kmedicine.ui.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.services.local.OrderDatabaseService
import com.dsu.kmedicine.services.local.UserDatabaseService
import com.dsu.kmedicine.services.remote.OrdersRemoteService
import com.dsu.kmedicine.utils.RequestCallback

class HomeViewModel(application: Application): AndroidViewModel(application) {

    val user = MutableLiveData<Response<UserResponse>>()
    val orders = MutableLiveData<Response<List<OrderResponse>>>()
    var initialOrders = listOf<OrderResponse>()
    val openOrderDetails = MutableLiveData<Response<OrderResponse>>()
    private val TAG = HomeViewModel::class.java.name

    fun checkUserExistence() {
        with(UserDatabaseService(getApplication())) {
            getUser(object : RequestCallback<UserResponse> {
                override fun onComplete(result: UserResponse) {
                    user.value = Response.Success(result)
                }

                override fun onException(e: Exception?) {
                    user.value = Response.DataError(e.toString())
                }
            })
        }
    }

    fun retrieveDataList() {
        retrieveOrdersFromDatabase()
    }

    private fun retrieveOrdersFromDatabase() {
        with(OrderDatabaseService(getApplication())) {
            getOrders(object : RequestCallback<List<OrderResponse>> {
                override fun onComplete(result: List<OrderResponse>) {
                    orders.value = Response.Success(result)
                    initialOrders = result
                    Log.e(TAG, "Succeeded on retrieving orders from remote")
                }

                override fun onException(e: Exception?) {
                    retrieveOrdersFromRemote()
                    Log.e(TAG, "Failed to retrieve orders from database")
                }

            })
        }
    }

    private fun retrieveOrdersFromRemote() {
        with(OrdersRemoteService()) {
            getOrders(object : RequestCallback<List<OrderResponse>> {
                override fun onComplete(result: List<OrderResponse>) {
                    orders.value = Response.Success(result)
                    initialOrders = result
                    insertOrdersIntoDatabase(result)
                }

                override fun onException(e: Exception?) {
                    Log.e(TAG, "Failed to get orders from remote")
                }
            })
        }
    }

    private fun insertOrdersIntoDatabase(result: List<OrderResponse>) {
        with(OrderDatabaseService(getApplication())) {
            setOrders(result, object : RequestCallback<List<OrderResponse>> {
                override fun onComplete(result: List<OrderResponse>) {
                    Log.e(TAG, "Succeed on inserting orders into database")
                }

                override fun onException(e: Exception?) {
                    Log.e(TAG, "Failed to insert orders into database")
                }
            })
        }
    }

    fun search(text: String) {
        if (text.isEmpty()) {
            orders.value = Response.Success(initialOrders)
        } else {
            val filteredOrders = initialOrders.filter { it.id.contains(text) }
            orders.value = Response.Success(filteredOrders)
        }
    }

    fun openOrderDetails(order: OrderResponse) {
        openOrderDetails.value = Response.Success(order)
    }
}
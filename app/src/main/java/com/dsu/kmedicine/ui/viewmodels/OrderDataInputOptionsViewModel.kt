package com.dsu.kmedicine.ui.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response

class OrderDataInputOptionsViewModel(application: Application): AndroidViewModel(application) {

    val order = MutableLiveData<Response<OrderResponse>>()

}
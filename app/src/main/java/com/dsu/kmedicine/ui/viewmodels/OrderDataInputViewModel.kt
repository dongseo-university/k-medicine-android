package com.dsu.kmedicine.ui.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dsu.kmedicine.model.data.ImageData
import com.dsu.kmedicine.model.response.MedicineResponse
import com.dsu.kmedicine.model.response.SuppliedDataResponse
import com.dsu.kmedicine.utils.InputDataType

class OrderDataInputViewModel(application: Application) : AndroidViewModel(application) {

    val medicines = MutableLiveData<List<MedicineResponse>>()
    val inputDataType = MutableLiveData<InputDataType>()
    val suppliedData = MutableLiveData<SuppliedDataResponse>()
    val cameraClickEvent = MutableLiveData<MutableList<ImageData>>()
    val imageClickEvent = MutableLiveData<ImageData>()
    val additionalInformation = MutableLiveData<String>()
    var isEditable = true

    fun onCameraClick(pictureList: MutableList<ImageData>) {
        cameraClickEvent.value = pictureList
    }

    fun addNewImageToTransaction(img: ImageData) {
        if (suppliedData.value == null) {
            suppliedData.value = SuppliedDataResponse(mutableListOf(img), "")
        } else {
            suppliedData.value?.pictureList?.add(img)
            suppliedData.value = suppliedData.value
        }
    }

    fun onImageClick(img: ImageData) {
        imageClickEvent.value = img
    }

}
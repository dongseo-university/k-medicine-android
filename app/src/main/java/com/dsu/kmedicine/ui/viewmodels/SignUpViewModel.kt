package com.dsu.kmedicine.ui.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.model.response.UserResponse
import com.dsu.kmedicine.services.local.UserDatabaseService
import com.dsu.kmedicine.utils.RequestCallback

class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    val user = MutableLiveData<Response<UserResponse>>()

    fun signUp(id: String, password: String, personInCharge: String) {
        val callback = object : RequestCallback<UserResponse> {
            override fun onComplete(result: UserResponse) {
                user.value = Response.Success(result)
            }
            override fun onException(e: Exception?) {
                user.value = Response.DataError(e.toString())
            }
        }

        if(id.isNotEmpty() && password.isNotEmpty() and personInCharge.isNotEmpty()) {
            val user = UserResponse(id, password, personInCharge)
            with(UserDatabaseService(getApplication())) {
                setUser(user, callback)
            }
        } else user.value = Response.DataError("")
    }

}
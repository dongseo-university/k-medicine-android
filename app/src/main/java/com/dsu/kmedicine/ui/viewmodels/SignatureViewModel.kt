package com.dsu.kmedicine.ui.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dsu.kmedicine.model.response.OrderResponse
import com.dsu.kmedicine.model.response.Response
import com.dsu.kmedicine.services.local.OrderDatabaseService
import com.dsu.kmedicine.utils.RequestCallback

class SignatureViewModel(application: Application): AndroidViewModel(Application()) {

    val order = MutableLiveData<Response<OrderResponse>>()
    val orderUpdatedEvent = MutableLiveData<Boolean>()
    private val TAG = SignatureViewModel::class.java.name

    fun updateOrderOnDatabase(orderDatabaseService: OrderDatabaseService) {
        order.value?.data?.let {
            with(orderDatabaseService) {
                updateOrder(it, object : RequestCallback<List<OrderResponse>> {
                    override fun onComplete(result: List<OrderResponse>) {
                        orderUpdatedEvent.value = true
                        Log.e(TAG, "Succeed on inserting orders into database")
                    }

                    override fun onException(e: Exception?) {
                        Log.e(TAG, "Failed to insert orders into database")
                    }
                })
            }
        }
    }

}
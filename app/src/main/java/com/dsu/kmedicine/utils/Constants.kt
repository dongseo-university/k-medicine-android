package com.dsu.kmedicine.utils

class Constants {
    companion object {
        const val DATABASE_NAME = "k-medicine-database"
        const val SHARED_PREFERENCES = "k-medicine-shared-preferences"
        const val USER_KEY = "k-medicine-user"
        const val ORDERS_KEY = "k-medicine-orders"

        // Intent constants
        const val ORDER_KEY = "ORDER_KEY"
        const val MEDICINE_KEY = "ORDER_KEY"
        const val SUPPLIED_DATA = "SUPPLIED_DATA_KEY"
        const val INPUT_DATA_TYPE = "INPUT_DATA_TYPE"
        const val SIGNATURE_IMAGE = "SIGNATURE_IMAGE"
        const val IS_INPUT_DATA_EDITABLE = "IS_INPUT_DATA_EDITABLE"
    }

    init {
        throw RuntimeException("Stub!")
    }
}
package com.dsu.kmedicine.utils

import androidx.annotation.DrawableRes
import com.dsu.kmedicine.R

enum class InputDataType(@DrawableRes val drawableRes: Int) {
    TAX_INVOICE(R.string.tax_invoice),
    TRANSACTION_STATEMENT(R.string.transaction_statement),
    TEMPERATURE_INFORMATION(R.string.temperature_information),
    CERTIFICATE_OF_SHIPMENT(R.string.certificate_of_shipment)
}
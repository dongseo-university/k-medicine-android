package com.dsu.kmedicine.utils

interface RequestCallback<T> {
    fun onComplete(result: T)
    fun onException(e: Exception?)
}